const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#0b101a", /* black   */
  [1] = "#236690", /* red     */
  [2] = "#4C7391", /* green   */
  [3] = "#3288B3", /* yellow  */
  [4] = "#4F8FB2", /* blue    */
  [5] = "#34A4D5", /* magenta */
  [6] = "#52B3DE", /* cyan    */
  [7] = "#aedbee", /* white   */

  /* 8 bright colors */
  [8]  = "#7999a6",  /* black   */
  [9]  = "#236690",  /* red     */
  [10] = "#4C7391", /* green   */
  [11] = "#3288B3", /* yellow  */
  [12] = "#4F8FB2", /* blue    */
  [13] = "#34A4D5", /* magenta */
  [14] = "#52B3DE", /* cyan    */
  [15] = "#aedbee", /* white   */

  /* special colors */
  [256] = "#0b101a", /* background */
  [257] = "#aedbee", /* foreground */
  [258] = "#aedbee",     /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
 unsigned int defaultrcs= 258;
