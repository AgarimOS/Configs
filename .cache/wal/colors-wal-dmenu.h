static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#aedbee", "#0b101a" },
	[SchemeSel] = { "#aedbee", "#236690" },
	[SchemeOut] = { "#aedbee", "#52B3DE" },
};
